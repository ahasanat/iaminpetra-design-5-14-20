<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div> 
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout">
   <div class="main-content with-lmenu hotels-page  main-page transheader-page">
      <div class="combined-column hotels-wrapper wide-open">
         <div class="content-box">
            <div class="banner-section">
               <div class="search-whole hotels-search container mt-0">
                  <div class="frow">
                     <div class="header-container">
                        <h3 class="header-text">Find the best flight</h3>
                     </div>
                     <div class="form-container">
                        <div class="row">
                           <div class="trip-type inline-radio">
                              <input name="tripType" checked="" type="radio" class="with-gap" id="round" value="round">
                              <label for="round">Round-trip</label>
                              <input name="tripType" type="radio" class="with-gap" id="oneway" value="oneway">
                              <label for="oneway">One-way</label>    
                           </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row row-option">
                           <div class="cols12">
                              <div class="col l3 m3 s6">
                                 <div class="sliding-middle-out input-field anim-area where-from dateinput fullwidth">
                                    <input type="text" placeholder="Where from?" class="form-control check-in" >
                                 </div>
                              </div>
                              <div class="col l3 m3 s6">
                                 <div class="sliding-middle-out input-field anim-area where-to dateinput fullwidth">
                                    <input type="text" placeholder="Where to?" class="form-control check-in">
                                 </div>
                              </div>
                              <div class="col l3 m3 s6">
                                 <div class="sliding-middle-out input-field anim-area dateinput fullwidth">
                                    <input type="text" placeholder="Departing - Returning" class="form-control check-out dept-return" data-query="M" data-toggle="datepicker" readonly>
                                 </div>
                              </div>
                              <div class="col l3 m3 s6">
                                 <div class="custom-drop">
                                    <div class="dropdown input-field dropdown-custom dropdown-xsmall">
                                       <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-rooms">
                                       <i class="mdi mdi-account"></i><span class=>1 Adult</span> <span class="mdi mdi-menu-down right caret"></span>
                                       </a>
                                       <ul class="dropdown-content" id="dropdown-rooms">
                                          <li><a href="javascript:void(0)"><span class="">1 Adult</a></li>
                                          <li><a href="javascript:void(0)"><i class=”mdi mdi-account-group”></i></i><span class="">2 Adults</span></a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row">
                           <div class="col s6 nonstop">
                              <p class="mt-0">
                                 <input type="checkbox" id="nonStop" />
                                 <label for="nonStop">
                                    <span class="stars-holder">Prefer nonstop</span>
                                 </label>
                              </p>
                           </div>
                           <div class="col s6">
                              <button class="btn-findflight">Find flights</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>

<?php include('common/datepicker.php'); ?>

<?php include("script.php"); ?>
<script type="text/javascript">

   $('.dept-return').daterangepicker();

</script>

</body>
</html>
