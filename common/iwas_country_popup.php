
<!--attachment modal-->
<div id="compose_iwasincountry" class="modal compose_inner_modal modalxii_level1 visit-country">
   <div class="content_header">
      <button class="close_span waves-effect">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <p class="selected_person_text">Select Destination</p>
   </div>
   <div class="person_box">
      <div class="collection visit-country-list">
         <a href="https://www.iaminjordan.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Jordan</a>
         <a href="https://www.iaminaqaba.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Aqaba</a>
         <a href="https://www.iaminmadain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Madain</a>
         <a href="https://www.iaminsaudi.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Saudi</a>
         <a href="https://www.iaminfrance.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in France</a>
         <a href="https://www.iamindubai.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Dubai</a>
         <a href="https://www.iaminspain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Spain</a>
         <a href="https://www.iaminqatar.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Qatar</a>
         <a href="https://www.iaminbahrain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Bahrain</a>
         <a href="https://www.iaminabudhabi.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Abu Dhabi</a>
         <a href="#!" class="collection-item">I am in USA</a>
         <a href="#!" class="collection-item">I am in Italy</a>
         <a href="#!" class="collection-item">I am in Mexico</a>
         <a href="#!" class="collection-item">I am in UK</a>
         <a href="#!" class="collection-item">I am in Istanbul</a>
         <a href="#!" class="collection-item">I am in China</a>
         <a href="#!" class="collection-item">I am in Germany</a>
         <a href="#!" class="collection-item">I am in Thailand</a>
         <a href="#!" class="collection-item">I am in Austria</a>
         <a href="#!" class="collection-item">I am in Hong Kong</a>
         <a href="#!" class="collection-item">I am in Greece</a>
         <a href="#!" class="collection-item">I am in Russia</a>
         <a href="#!" class="collection-item">I am in Petra</a>
         <a href="#!" class="collection-item">I am in Japan</a>
         <a href="#!" class="collection-item">I am in Cyprus</a>
         <a href="#!" class="collection-item">I am in Jamaica</a>
         <a href="#!" class="collection-item">I am in Poland</a>
         <a href="#!" class="collection-item">I am in Hungary</a>
         <a href="#!" class="collection-item">I am in Zimbabwe</a>
         <a href="#!" class="collection-item">I am in Ukraine</a>
         <a href="#!" class="collection-item">I am in Netherlands</a>
         <a href="#!" class="collection-item">I am in India</a>
         <a href="#!" class="collection-item">I am in Singapore</a>
         <a href="#!" class="collection-item">I am in Morocco</a>
         <a href="#!" class="collection-item">I am in Egypt</a>
         <a href="#!" class="collection-item">I am in Brazil</a>
         <a href="#!" class="collection-item">I am in Australia</a>
         <a href="#!" class="collection-item">I am in Malaysia</a>
         <a href="#!" class="collection-item">I am in Indonesia</a>
         <a href="#!" class="collection-item">I am in Argentina</a>
         <a href="#!" class="collection-item">I am in Zimbabwe</a>
         <a href="#!" class="collection-item">I am in Macao</a>
         <a href="#!" class="collection-item">I am in Croatia</a>
         <a href="#!" class="collection-item">I am in Southkorea</a>
      </div>
   </div>
</div>
