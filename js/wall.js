/************ WALL FUNCTIONS ************/
$country = {"AFGHANISTAN" : "AFGHANISTAN", "ALBANIA" : "ALBANIA", "ALGERIA" : "ALGERIA", "AMERICAN SAMOA" : "AMERICAN SAMOA", "ANDORRA" : "ANDORRA", "ANGOLA" : "ANGOLA", "ANGUILLA" : "ANGUILLA", "ANTARCTICA" : "ANTARCTICA", "ANTIGUA AND BARBUDA" : "ANTIGUA AND BARBUDA", "ARGENTINA" : "ARGENTINA", "ARMENIA" : "ARMENIA", "ARUBA" : "ARUBA", "AUSTRALIA" : "AUSTRALIA", "AUSTRIA" : "AUSTRIA", "AZERBAIJAN" : "AZERBAIJAN", "BAHAMAS" : "BAHAMAS", "BAHRAIN" : "BAHRAIN", "BANGLADESH" : "BANGLADESH", "BARBADOS" : "BARBADOS", "BELARUS" : "BELARUS", "BELGIUM" : "BELGIUM", "BELIZE" : "BELIZE", "BENIN" : "BENIN", "BERMUDA" : "BERMUDA", "BHUTAN" : "BHUTAN", "BOLIVIA" : "BOLIVIA", "BOSNIA AND HERZEGOVINA" : "BOSNIA AND HERZEGOVINA", "BOTSWANA" : "BOTSWANA", "BOUVET ISLAND" : "BOUVET ISLAND", "BRAZIL" : "BRAZIL", "BRITISH INDIAN OCEAN TERRITORY" : "BRITISH INDIAN OCEAN TERRITORY", "BRUNEI DARUSSALAM" : "BRUNEI DARUSSALAM", "BULGARIA" : "BULGARIA", "BURKINA FASO" : "BURKINA FASO", "BURUNDI" : "BURUNDI", "CAMBODIA" : "CAMBODIA", "CAMEROON" : "CAMEROON", "CANADA" : "CANADA", "CAPE VERDE" : "CAPE VERDE", "CAYMAN ISLANDS" : "CAYMAN ISLANDS", "CENTRAL AFRICAN REPUBLIC" : "CENTRAL AFRICAN REPUBLIC", "CHAD" : "CHAD", "CHILE" : "CHILE", "CHINA" : "CHINA", "CHRISTMAS ISLAND" : "CHRISTMAS ISLAND", "COCOS (KEELING) ISLANDS" : "COCOS (KEELING) ISLANDS", "COLOMBIA" : "COLOMBIA", "COMOROS" : "COMOROS", "CONGO" : "CONGO", "CONGO, THE DEMOCRATIC REPUBLIC OF THE" : "CONGO, THE DEMOCRATIC REPUBLIC OF THE", "COOK ISLANDS" : "COOK ISLANDS", "COSTA RICA" : "COSTA RICA", "COTE D'IVOIRE" : "COTE D'IVOIRE", "CROATIA" : "CROATIA", "CUBA" : "CUBA", "CYPRUS" : "CYPRUS", "CZECH REPUBLIC" : "CZECH REPUBLIC", "DENMARK" : "DENMARK", "DJIBOUTI" : "DJIBOUTI", "DOMINICA" : "DOMINICA", "DOMINICAN REPUBLIC" : "DOMINICAN REPUBLIC", "ECUADOR" : "ECUADOR", "EGYPT" : "EGYPT", "EL SALVADOR" : "EL SALVADOR", "EQUATORIAL GUINEA" : "EQUATORIAL GUINEA", "ERITREA" : "ERITREA", "ESTONIA" : "ESTONIA", "ETHIOPIA" : "ETHIOPIA", "FALKLAND ISLANDS (MALVINAS)" : "FALKLAND ISLANDS (MALVINAS)", "FAROE ISLANDS" : "FAROE ISLANDS", "FIJI" : "FIJI", "FINLAND" : "FINLAND", "FRANCE" : "FRANCE", "FRENCH GUIANA" : "FRENCH GUIANA", "FRENCH POLYNESIA" : "FRENCH POLYNESIA", "FRENCH SOUTHERN TERRITORIES" : "FRENCH SOUTHERN TERRITORIES", "GABON" : "GABON", "GAMBIA" : "GAMBIA", "GEORGIA" : "GEORGIA", "GERMANY" : "GERMANY", "GHANA" : "GHANA", "GIBRALTAR" : "GIBRALTAR", "GREECE" : "GREECE", "GREENLAND" : "GREENLAND", "GRENADA" : "GRENADA", "GUADELOUPE" : "GUADELOUPE", "GUAM" : "GUAM", "GUATEMALA" : "GUATEMALA", "GUINEA" : "GUINEA", "GUINEA-BISSAU" : "GUINEA-BISSAU", "GUYANA" : "GUYANA", "HAITI" : "HAITI", "HEARD ISLAND AND MCDONALD ISLANDS" : "HEARD ISLAND AND MCDONALD ISLANDS", "HOLY SEE (VATICAN CITY STATE)" : "HOLY SEE (VATICAN CITY STATE)", "HONDURAS" : "HONDURAS", "HONG KONG" : "HONG KONG", "HUNGARY" : "HUNGARY", "ICELAND" : "ICELAND", "INDIA" : "INDIA", "INDONESIA" : "INDONESIA", "IRAN, ISLAMIC REPUBLIC OF" : "IRAN, ISLAMIC REPUBLIC OF", "IRAQ" : "IRAQ", "IRELAND" : "IRELAND", "ISRAEL" : "ISRAEL", "ITALY" : "ITALY", "JAMAICA" : "JAMAICA", "JAPAN" : "JAPAN", "JORDAN" : "JORDAN", "KAZAKHSTAN" : "KAZAKHSTAN", "KENYA" : "KENYA", "KIRIBATI" : "KIRIBATI", "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF" : "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF", "KOREA, REPUBLIC OF" : "KOREA, REPUBLIC OF", "KUWAIT" : "KUWAIT", "KYRGYZSTAN" : "KYRGYZSTAN", "LAO PEOPLE'S DEMOCRATIC REPUBLIC" : "LAO PEOPLE'S DEMOCRATIC REPUBLIC", "LATVIA" : "LATVIA", "LEBANON" : "LEBANON", "LESOTHO" : "LESOTHO", "LIBERIA" : "LIBERIA", "LIBYAN ARAB JAMAHIRIYA" : "LIBYAN ARAB JAMAHIRIYA", "LIECHTENSTEIN" : "LIECHTENSTEIN", "LITHUANIA" : "LITHUANIA", "LUXEMBOURG" : "LUXEMBOURG", "MACAO" : "MACAO", "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF" : "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF", "MADAGASCAR" : "MADAGASCAR", "MALAWI" : "MALAWI", "MALAYSIA" : "MALAYSIA", "MALDIVES" : "MALDIVES", "MALI" : "MALI", "MALTA" : "MALTA", "MARSHALL ISLANDS" : "MARSHALL ISLANDS", "MARTINIQUE" : "MARTINIQUE", "MAURITANIA" : "MAURITANIA", "MAURITIUS" : "MAURITIUS", "MAYOTTE" : "MAYOTTE", "MEXICO" : "MEXICO", "MICRONESIA, FEDERATED STATES OF" : "MICRONESIA, FEDERATED STATES OF", "MOLDOVA, REPUBLIC OF" : "MOLDOVA, REPUBLIC OF", "MONACO" : "MONACO", "MONGOLIA" : "MONGOLIA", "MONTSERRAT" : "MONTSERRAT", "MOROCCO" : "MOROCCO", "MOZAMBIQUE" : "MOZAMBIQUE", "MYANMAR" : "MYANMAR", "NAMIBIA" : "NAMIBIA", "NAURU" : "NAURU", "NEPAL" : "NEPAL", "NETHERLANDS" : "NETHERLANDS", "NETHERLANDS ANTILLES" : "NETHERLANDS ANTILLES", "NEW CALEDONIA" : "NEW CALEDONIA", "NEW ZEALAND" : "NEW ZEALAND", "NICARAGUA" : "NICARAGUA", "NIGER" : "NIGER", "NIGERIA" : "NIGERIA", "NIUE" : "NIUE", "NORFOLK ISLAND" : "NORFOLK ISLAND", "NORTHERN MARIANA ISLANDS" : "NORTHERN MARIANA ISLANDS", "NORWAY" : "NORWAY", "OMAN" : "OMAN", "PAKISTAN" : "PAKISTAN", "PALAU" : "PALAU", "PALESTINIAN TERRITORY, OCCUPIED" : "PALESTINIAN TERRITORY, OCCUPIED", "PANAMA" : "PANAMA", "PAPUA NEW GUINEA" : "PAPUA NEW GUINEA", "PARAGUAY" : "PARAGUAY", "PERU" : "PERU", "PHILIPPINES" : "PHILIPPINES", "PITCAIRN" : "PITCAIRN", "POLAND" : "POLAND", "PORTUGAL" : "PORTUGAL", "PUERTO RICO" : "PUERTO RICO", "QATAR" : "QATAR", "REUNION" : "REUNION", "ROMANIA" : "ROMANIA", "RUSSIAN FEDERATION" : "RUSSIAN FEDERATION", "RWANDA" : "RWANDA", "SAINT HELENA" : "SAINT HELENA", "SAINT KITTS AND NEVIS" : "SAINT KITTS AND NEVIS", "SAINT LUCIA" : "SAINT LUCIA", "SAINT PIERRE AND MIQUELON" : "SAINT PIERRE AND MIQUELON", "SAINT VINCENT AND THE GRENADINES" : "SAINT VINCENT AND THE GRENADINES", "SAMOA" : "SAMOA", "SAN MARINO" : "SAN MARINO", "SAO TOME AND PRINCIPE" : "SAO TOME AND PRINCIPE", "SAUDI ARABIA" : "SAUDI ARABIA", "SENEGAL" : "SENEGAL", "SERBIA AND MONTENEGRO" : "SERBIA AND MONTENEGRO", "SEYCHELLES" : "SEYCHELLES", "SIERRA LEONE" : "SIERRA LEONE", "SINGAPORE" : "SINGAPORE", "SLOVAKIA" : "SLOVAKIA", "SLOVENIA" : "SLOVENIA", "SOLOMON ISLANDS" : "SOLOMON ISLANDS", "SOMALIA" : "SOMALIA", "SOUTH AFRICA" : "SOUTH AFRICA", "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS" : "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS", "SPAIN" : "SPAIN", "SRI LANKA" : "SRI LANKA", "SUDAN" : "SUDAN", "SURINAME" : "SURINAME", "SVALBARD AND JAN MAYEN" : "SVALBARD AND JAN MAYEN", "SWAZILAND" : "SWAZILAND", "SWEDEN" : "SWEDEN", "SWITZERLAND" : "SWITZERLAND", "SYRIAN ARAB REPUBLIC" : "SYRIAN ARAB REPUBLIC", "TAIWAN, PROVINCE OF CHINA" : "TAIWAN, PROVINCE OF CHINA", "TAJIKISTAN" : "TAJIKISTAN", "TANZANIA, UNITED REPUBLIC OF" : "TANZANIA, UNITED REPUBLIC OF", "THAILAND" : "THAILAND", "TIMOR-LESTE" : "TIMOR-LESTE", "TOGO" : "TOGO", "TOKELAU" : "TOKELAU", "TONGA" : "TONGA", "TRINIDAD AND TOBAGO" : "TRINIDAD AND TOBAGO", "TUNISIA" : "TUNISIA", "TURKEY" : "TURKEY", "TURKMENISTAN" : "TURKMENISTAN", "TURKS AND CAICOS ISLANDS" : "TURKS AND CAICOS ISLANDS", "TUVALU" : "TUVALU", "UGANDA" : "UGANDA", "UKRAINE" : "UKRAINE", "UNITED ARAB EMIRATES" : "UNITED ARAB EMIRATES", "UNITED KINGDOM" : "UNITED KINGDOM", "UNITED STATES" : "UNITED STATES", "UNITED STATES MINOR OUTLYING ISLANDS" : "UNITED STATES MINOR OUTLYING ISLANDS", "URUGUAY" : "URUGUAY", "UZBEKISTAN" : "UZBEKISTAN", "VANUATU" : "VANUATU", "VENEZUELA" : "VENEZUELA", "VIET NAM" : "VIET NAM", "VIRGIN ISLANDS, BRITISH" : "VIRGIN ISLANDS, BRITISH", "VIRGIN ISLANDS, U.S." : "VIRGIN ISLANDS, U.S.", "WALLIS AND FUTUNA" : "WALLIS AND FUTUNA", "WESTERN SAHARA" : "WESTERN SAHARA", "YEMEN" : "YEMEN", "ZAMBIA" : "ZAMBIA", "ZIMBABWE" : "ZIMBABWE"};

$(window).resize(function() {
	if($('#about-content').hasClass('active')) {
		zhijsd();
	}
}); 

$(document).on('click', '.right.editicon', function() {
	if($('#about-content').hasClass('active')) {
		zhijsd();
	}
});

$(document).ready(function() {			
	$(document).on('click', '.callfiximageui', function() {
		setTimeout(function() { 
			fixImageUI('wall-photoalbums'); 
		}, 200);
	});

	
	getCountries();

	/* popup tag change - change cover/photo */
	$("body").on("click", ".pc-section .nav li", pcSectionTabChange);				
	/* end popup tag change - change cover/photo */		
		
	var isWall=$(".page-wrapper").hasClass("wallpage");
	if(isWall){	
		/*	
		$('#albumCarousel').carousel({
			interval: false //changes the speed
		});
		*/
		/* album slider */
			initPhotoCarousel("wallpage");	  					  
		/* end album slider */
	}

	/* reset photos - photos page : second tab */
	$("body").on("click", ".photos-content .album-tab", resetAlbumsTab);
	/* end reset photos - photos page : second tab */

	/* open saved post */
	$("body").on("click", ".saved-post .s_postlink", openSavedPost);		
	/* end open saved post */
	
	/* album carousel initalization */
	$("body").on("click", ".carousel-albums .descholder .dropdown-toggle",function(){			
		$('.carousel-albums .carousel-inner').css("overflow","visible");
	});
	/* end album carousel initalization */

	/* wall page : send message section */
	$("body").on("click", ".canceladdchat", function(e){
		closeAddNewMsg();
	});
	$("body").on("click", ".doneaddchat", function(e){
		closeAddNewMsg();
	});
	/* end wall page : send message section */
	
	/* open activity post */
		fixImageUI("activity");
		$("body").on("click", ".activity-holder .a_postlink", openActivityPost);		
	/* end open activity post */
	
	/* swich wall main tabs */
	$(".link-menu li a").click(function(){
		if($(this).hasClass("activity-link")){
			if($(".page-wrapper").hasClass("wallpage")){				
				if($("#about-content").find(".mode-holder").hasClass("opened")){$("#about-content").find(".mode-holder").removeClass("opened");}
				$("#about-content").removeClass("active in");
			}
			
			resetTabs(".custom-wall-links");
			resetTabs(".sub-tabs");
			
			$(".fixed-layout").addClass("hide-addflow");
		
			setTimeout(function(){
				fixImageUI("activity");
			},400);
			
			var sparent = $(".main-content");
			if(sparent.hasClass("gone")){
				sparent.removeClass("gone");
			}
			setTimeout(function(){				
				resetInnerPage("wall","hide");
			},600);
		}
	});
	
	$(".custom-wall-links li a").click(function(){	
		resetTabs(".link-menu");	
		wallTabClicked(this,"custom-walllinks");
	});
	/* end swich wall main tabs */
	
	/* swich wall sub tabs */			
	$(".sub-tabs li a").click(function(){		
		resetTabs(".link-menu");
		wallTabClicked(this,"subtabs");
	});			
	/* end swich wall sub tabs */
});

function zhijsd() {
	$selectore1 = $('#about-content').find('.birth_date_privacy');
	$selectore2 = $('#about-content').find('.gender_privacy');

	var winw = $(window).width();
	if(winw <= 480) {
		$selectore1.attr('onclick', 'privacymodal(this)');
		$selectore1.attr('data-activates', '');
		$selectore1.parents('.left').find('ul').addClass('forcefullyhide');

		$selectore2.attr('onclick', 'privacymodal(this)');
		$selectore2.attr('data-activates', '');
		$selectore2.parents('.left').find('ul').addClass('forcefullyhide');
	} else {
		$data_activities1 = $selectore1.parents('.left').find('ul').attr('id');
		$data_activities2 = $selectore2.parents('.left').find('ul').attr('id');

		$selectore1.attr('data-activates', $data_activities1);
		$selectore1.attr('onclick', '');
		$selectore1.parents('.left').find('ul').removeClass('forcefullyhide');
		$selectore1.parents('.left').find('ul').hide();

		$selectore2.attr('data-activates', $data_activities2);
		$selectore2.attr('onclick', '');
		$selectore2.parents('.left').find('ul').removeClass('forcefullyhide');
		$selectore2.parents('.left').find('ul').hide();

		$('.dropdown-button').dropdown();
	}
}

/* FUN reset tabs */
	function resetWallTabs(linktext,toWhich){
		var sparent;
		if(toWhich=="custom-walllinks"){
			sparent=".custom-wall-links";			
		}else{
			sparent=".sub-tabs";			
		}
		$(sparent+" ul li").each(function(){
			if($(this).hasClass("active")){
				$(this).removeClass("active");
			}
			if($(this).find("a").attr("href")==linktext){
				$(this).addClass("active");
			}
		});
	}

	function wallTabClicked(obj,fromWhere) {
		$(".page-wrapper .header-themebar").addClass("innerpage");

		var ahref= $(obj).attr("href");
		var tabname = $(obj).attr("tabname");
		
		if(tabname!="Wall" && tabname!="Post"){
			var sparent = $(".main-content");
			if(sparent.hasClass("gone")){
				sparent.removeClass("gone");
			}
			resetInnerPage("wall","hide");
		}

		if(fromWhere=="subtabs"){
			resetWallTabs(ahref,"custom-walllinks");
		}else{
			resetWallTabs(ahref,"subtabs");			
		}
		
		if(tabname!="Wall" && tabname!="Refers" && tabname!="References" && tabname!="Reviews"){
			manageNewPostBubble("hideit");			
		}else{	
			
			if($(obj).hasClass("disabled"))
				manageNewPostBubble("hideit");
			else
				manageNewPostBubble("showit");
		}
		
		$('.user-photos').removeClass('photoshow');
		
		if(tabname=="Photos"){
						 
			if($("#photos-content").find(".user-photos").length > 0){
				var win_w = $(window).width();
				var settime = 400;
				if(win_w < 768)
					settime = 700;
				
				if (photocount < 1){
					setTimeout(function(){					 
						photocount = photocount + 1;
						$('.user-photos').addClass('photoshow');
					},settime);
				}else
					$('.user-photos').addClass('photoshow');
				
				setTimeout(function(){
					fixImageUI("wall-photoslider");
				},400);
				
			}
			
			setTimeout(function(){
				fixImageUI("wall-photoalbums");	
				initGalleryImageSlider();
			},800);
			
		}
				
		if(tabname=="Contributions"){
			
			setTimeout(function(){
				$('.gauge2').gauge({
					values: {
						0 : '0',
						20: '25 - 75',
						100: '100'
					},
					colors: {
						0 : '#ED1C24',
						20 : '#39B54A',
						80: '#FDD914'
					},
					angles: [
						180,
						360
					],
					lineWidth: 20,
					arrowWidth: 10,
					arrowColor: '#ccc',
					inset:true,

					value: 65
				});
			},400);
		}
		
		$("body").getNiceScroll().resize();		
		if(tabname=="Gallery"){	
			setTimeout(function(){
				$(".gloader").hide();
				initNiceScroll(".nice-scroll");
				justifiedGalleryinitialize();
				lightGalleryinitialize();
			},700);
		}
		
		setTimeout(function(){fixImageUI();setGeneralThings();},400);
		
		var initScroll = 190;
		var win_w = $(window).width();
		
		if(win_w < 767)
			initScroll = 0;
		
		
		$('html, body').animate({ scrollTop: initScroll }, 'slow');
	}
	function resetTabs(sparent){
		$(sparent+" li").each(function(){
			$(this).removeClass("active");
		});		
	}
/* FUN end reset tabs */

/* FUN change cover/photo */
	function resetAllPopupSection(fromwhere){
		if(fromwhere=="cover"){
			$(".popup-area#change-cover .pc-section").hide();
			$(".popup-area#change-cover .pc-title").hide();
		}
		else{
			$(".popup-area#change-profile .pc-section").hide();
			$(".popup-area#change-profile .pc-title").hide();
		}
	}
	function changePopup(fromwhere, which){
		resetAllPopupSection(fromwhere);
		$(".popup-area#change-profile").removeClass("uploadphoto-popup");
		if(fromwhere=="cover"){			
			$(".popup-area#change-cover ."+which).show();

			if(which=="choosephoto"){
				setTimeout(function(){fixImageUIPopup();},400);
			}
		}
		else{
			$(".popup-area#change-profile ."+which).show();
			if(which=="uploadphoto"){
				$(".popup-area#change-profile").addClass("uploadphoto-popup");
			}
		}
	}
	function openDefaultSlider(){
		$(".cover-slider").addClass("openDrawer");
	}
	function closeDefaultSlider(){
		$(".cover-slider").removeClass("openDrawer");
	}
/* FUN end change cover/photo */

/* FUN imagefix class - manage in popup tab */
	function pcSectionTabChange(){
		
		if(!$(this).hasClass("visited")){			
			setTimeout(function(){
				fixImageUIPopup();
				var tabpan_id=($(this).find("a").attr("href")).replace("#","");			
				$(".pic-change-popup").find("#"+tabpan_id).addClass("visited");
			},100);
			$(this).addClass("visited");
		}
	}
/* FUN end imagefix class - manage in popup tab */

/* FUN open album images */
	function openAlbumImages(obj){
		var super_parent=$(obj).parents(".tab-pane");
		var tabpane=$(obj).parents(".tab-pane").attr("id");
		
		if(tabpane=="pc-albums"){		
			$("#"+tabpane).find(".photos-area").show();
			$("#"+tabpane).find(".albums-area").hide();
		}
		if(tabpane=="pc-all"){
			/* replace photos from ajax call */			
		}
	}
/* FUN end open album images */

/* FUN hide all saved post detail */
	function hideAllPostDetails(){
		$(".saved-post .saved-post-detail").each(function(){			
			$(this).hide();
		});
	}
/* FUN end hide all saved post detail */

/* FUN open saved post */
	function openSavedPost(){
	
		var super_parent=$(this).parents(".main-li");
		if(super_parent.find(".saved-post-detail").css("display")=="none"){
			hideAllPostDetails();
			super_parent.find(".saved-post-detail").slideDown();
			setTimeout(function(){			
				fixImageUI("openSavedPost");
			},300);
		}
		else{
			super_parent.find(".saved-post-detail").slideUp();			
		}
	
	}
/* FUN end open saved post */

/* FUN reset albums tab */
	function resetAlbumsTab(){
		$("#pc-albums").find(".photos-area").hide();
		$("#pc-albums").find(".photos-area").html();
		$("#pc-albums").find(".albums-area").show();
	}
/* FUN end reset albums tab */

/* manage wall tab scroll */
	function manageWallSubTabScroll(){
		
		var win_w=$(window).width();		
		
		if(win_w > 650){
			$(".tab-scroll").getNiceScroll().remove();
			$(".tab-scroll").css({"width":"auto","overflow":"initial"});
		}
		else{
			$(".tab-scroll").niceScroll();
		}
	}
/* end manage wall tab scroll */

/* FUN to focus on comment area */
	function commentFocus()
	{  
		 var comid=$(this).attr("data-id");
		 var id = $("#comment_txt_"+comid);
		 titleUnderline($(id).parent());
		 $(id).focus();
		 
		 if($(this).parents(".reviewpost-holder")){
			$(this).parents(".post-data").find(".comments-area").slideDown();
		}
	}
/* FUN to focus on comment area */

/* FUN initalize photo carousel */ 
	function resetPhotoCarousel(parentClass){
		$(parentClass+" .item div[class*='cloneditem-']").remove();		
	}
	function initPhotoCarousel(which){
		
		var parentClass="";
		if(which == "wallpage"){
			parentClass=".photos-content .carousel-albums";			
		}
		if(which == "topattractions"){
			parentClass=".topattractions-section .carousel-albums";			
		}
		if(which == "topplaces"){
			parentClass=".topplaces-section .carousel-albums";			
		}
		resetPhotoCarousel(parentClass);
		$(parentClass).carousel({interval:false});
		
		var win_w=$(window).width();
		
		var ci_count = $(parentClass+" .item").length;
		var ci_limit=3;
		
		if(win_w>=600){
			ci_limit=3;		
		}else if(win_w>=450 && win_w<600){
			ci_limit=2;
		}else{
			ci_limit=1;
		}
	   if(ci_count >ci_limit ){
		   
		   if($(parentClass).hasClass("no-carousel"))
				$(parentClass).removeClass("no-carousel");
			
		   $(parentClass+" .carousel-control").show();  

		   $(parentClass+' .item').each(function(){
				var itemToClone = $(this);

				for (var i=1;i<ci_limit;i++) {
				  itemToClone = itemToClone.next();

				  // wrap around if at end of item collection
				  if (!itemToClone.length) {
					itemToClone = $(this).siblings(':first');
				  }

				  // grab item, clone, add marker class, add to collection
				  itemToClone.children(':first-child').clone()
				 .addClass("cloneditem-"+(i))
				 .appendTo($(this));
				}
		   });
	   }
	   else{
		$(parentClass).addClass("no-carousel");
		$(parentClass+" .carousel-control").hide();
	   }
	 
	 }
/* FUN end initalize photo carousel */

/* FUN set wall stuff edit mode textarea */
	function setWallEditTextarea(obj){		
		var nearestMainParent=$(obj).parents(".swapping-parent");		
		var tt=nearestMainParent.find(".edit-mode").find("textarea");		
	}
/* FUN end set wall stuff edit mode textarea */

/* FUN open about section of wall */	
	function openAboutSection(state){
		
		if($(".page-wrapper").hasClass("wallpage")){
			if($(".page-wrapper").hasClass("businesspage")){
				closePageSettings();
			}
			var findli = $(".sub-tabs .nav-tabs li a[href='#about-content']");		
			findli.trigger("click");
			if(state=="normal"){
				if($("#about-content").find(".mode-holder").hasClass("opened")){
					$("#about-content").find(".mode-holder").removeClass("opened");
				}
			}else{
				$("#about-content").find(".mode-holder").addClass("opened");			
			}
		}
	}
	function openWallTabInternally(which){
		
		if($(".page-wrapper").hasClass("wallpage")){
			if($(".page-wrapper").hasClass("businesspage")){
				closePageSettings();
			}
			var findli = $(".sub-tabs .nav-tabs li a[href='#"+which+"']");		
			findli.trigger("click");
						
		}
	}
/* FUN end open about section of wall */

/* FUN open send message drawer - wall page */
	function showMsg(obj){
		var win_w = $(window).width();
		if(win_w > 799) {
			var sparent = $(obj).parents(".wall-header");
			var msgObj = sparent.find('.sendMessage');
			if(msgObj.hasClass('open')) {
				msgObj.hide();
				msgObj.removeClass('open');
			} else {
				msgObj.show();
				msgObj.addClass('open');
			}
		} else {	 
			activemessageblock();	
			/*var msgObj = $("#newmsg-popup").find('.sendMessage');
			msgObj.show();		*/	
		}
	}
/* FUN end open send message drawer - wall page */

/* wall refers functions */
	function replyEditMode(obj){
		
		var sparent=$(obj).parents(".feedback-reply");
		
		sparent.find(".normal-mode").hide();
		sparent.find(".edit-mode").show();
	}
	function replyNormalMode(obj){
		
		var sparent=$(obj).parents(".feedback-reply");
		
		sparent.find(".normal-mode").show();
		sparent.find(".edit-mode").hide();
	}
	function replyDelete(obj){
		
		var mparent=$(obj).parents(".refer-post");
		var sparent=$(obj).parents(".feedback-reply");
		
		if(mparent.hasClass("replied")){
			mparent.removeClass("replied");
			sparent.remove();
		}
	}
/* end wall refers functions */

/* FUN places tab dropdown, this code in code folder we have move in places.js file */
	function placesTabDropdown(name, evt) {
		
		if(evt.params.data.id=="all")
			$('.text-menu a[href="#places-all"]').trigger('click');
		
		if(evt.params.data.id=="reviews")		
			$('.text-menu a[href="#places-reviews"]').trigger('click');
		
		if(evt.params.data.id=="travellers")		
			$('.text-menu a[href="#places-travellers"]').trigger('click');
		
		if(evt.params.data.id=="locals")		
			$('.text-menu a[href="#places-locals"]').trigger('click');
		
		if(evt.params.data.id=="photos")		
			$('.text-menu a[href="#places-photos"]').trigger('click');
		
		if(evt.params.data.id=="guides")		
			$('.text-menu a[href="#places-guides"]').trigger('click');
		
		if(evt.params.data.id=="hangout")		
			$('.text-menu a[href="#places-hangout"]').trigger('click');
		
		if(evt.params.data.id=="events")		
			$('.text-menu a[href="#places-events"]').trigger('click');
		
		if(evt.params.data.id=="tip")		
			$('.text-menu a[href="#places-tip"]').trigger('click');
		
		if(evt.params.data.id=="ask")		
			$('.text-menu a[href="#places-ask"]').trigger('click');
				
	}
/* FUN end places tab dropdown */

/* FUN open activity post */
	function openActivityPost(){		
		var super_parent=$(this).parents(".main-li");
		if(super_parent.find(".activity-post-detail").css("display")=="none"){
			hideAllPostDetails();
			super_parent.find(".activity-post-detail").slideDown();
			setTimeout(function(){			
				fixImageUI("openActivityPost");
			},300);
		}
		else{
			super_parent.find(".activity-post-detail").slideUp();			
		}
	
	}
/* FUN end open activity post */

/* manage resizable holder */
	function openResizable(obj){
		var holder=$(obj).parents(".resizable-holder");
		holder.addClass("expanded");
	}
/* end manage resizable holder */

/* destinations listing */
	function destListing(){
		$("#destigrid").css("visibility","hidden");
		$('#destigrid').waterfall({gridWidth : [0,400,800]});
		setTimeout(function(){
			$("#destigrid").css("visibility","visible");$(".gloader").hide();
			initNiceScroll(".nice-scroll");
		},700);			
	}
/* end destinations listing */
/* destinations listing */
	function box(){
		setTimeout(function(){
			$(".gloader").hide();
			initNiceScroll(".nice-scroll");
			justifiedGalleryinitialize();
			lightGalleryinitialize();
		},700);
	}
/* end destinations listing */

/* wall page - open settings tabs in mobile */
	function backToMain(fromWhere){
		
		if(fromWhere == "businesspage"){
			var contentID = $(".main-content .main-tabpane.active").attr("id");
			
			if(contentID == "messages-content"){
				closeAddNewMsg(); // back to messages screen
			}
			if(contentID == "settings-content"){
				getBack(); // back to settings screen
			}
		}
		
	}
	function getBack(){		
		$(".settings-content .open-innerpage").find(".tabs-detail").addClass("gone");		
		
		$(".header-themebar .mbl-innerhead").hide();
		$(".header-themebar .mbl-innerhead").find('.page-name').html("");		
	}
/* end wall page - open settings tabs in mobile */

/************ END WALL FUNCTIONS ************/

function getCountries()
{	  
  	$('.chips-initial1').material_chip({
		//data: $visited_countries,
	    autocompleteOptions: {
	      data: $country,
	      limit: Infinity,
	      minLength: 1
	    }
  	});

  	$('.chips-initial2').material_chip({
		//data: $lived_countries,
	    autocompleteOptions: {
	      data: $country,
	      limit: Infinity,
	      minLength: 1
	    }
  	});
}
